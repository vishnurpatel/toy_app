class User < ActiveRecord::Base
has_many :microposts
validates :name, length: { maximum: 15,minimum:4 }, presence: true
validates :email, length: { maximum: 30,minimum:6 }, presence: true
end